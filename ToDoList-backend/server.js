const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const port = 3000;

app.use(cors());
app.use(bodyParser.json());

// Dummy in-memory database
let tasks = [];

// Routes
app.get('/tasks', (req, res) => {
  res.json(tasks);
});

app.post('/tasks', (req, res) => {
  const newTask = req.body;
  newTask.id = Date.now();
  tasks.push(newTask);
  res.json(newTask);
});

app.delete('/tasks/:id', (req, res) => {
  const taskId = parseInt(req.params.id);
  tasks = tasks.filter((task) => task.id !== taskId);
  res.json({ success: true });
});

app.get('/completed-tasks', (req, res) => {
    const completedTasks = tasks.filter((task) => task.completed);
    res.json(completedTasks);
  });
  
  app.delete('/completed-tasks', (req, res) => {
    tasks = tasks.filter((task) => !task.completed);
    res.json({ success: true });
  });
app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});
