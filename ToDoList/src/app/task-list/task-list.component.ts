// task-list.component.ts
import { Component, OnInit } from '@angular/core';
import { TaskService } from '../task.service';
import { Task } from '../task.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css'],
})
export class TaskListComponent implements OnInit {
  tasks$: Observable<Task[]>;
  completedTasks$: Observable<Task[]>;

  constructor(public taskService: TaskService) {
    this.tasks$ = this.taskService.getTasks();
    this.completedTasks$ = this.taskService.getCompletedTasks();
  }

  ngOnInit() {
    // Initialize your component or perform additional logic here if needed
  }

  addTask(title: string, category: string): void {
    if (title.trim()) {
      this.taskService.addTask({
        id: Date.now(),
        title,
        completed: false,
        category,
      }).subscribe(() => {
        // Refresh the tasks after adding a new task
        this.tasks$ = this.taskService.getTasks();
        this.completedTasks$ = this.taskService.getCompletedTasks();
      });
    }
  }

  removeTask(task: Task): void {
    this.taskService.removeTask(task.id).subscribe(() => {
      // Refresh the tasks after removing a task
      this.tasks$ = this.taskService.getTasks();
      this.completedTasks$ = this.taskService.getCompletedTasks();
    });
  }

  clearCompletedTasks(): void {
    this.taskService.clearCompletedTasks().subscribe(() => {
      // Refresh the tasks after clearing completed tasks
      this.tasks$ = this.taskService.getTasks();
      this.completedTasks$ = this.taskService.getCompletedTasks();
    });
  }
}
