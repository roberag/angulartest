// task.service.ts
import { Injectable } from '@angular/core';
import { Task } from './task.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TaskService {
  private apiUrl = 'http://localhost:3000/tasks';
  private completedTasksUrl = 'http://localhost:3000/completed-tasks';

  constructor(private http: HttpClient) {}

  getTasks(): Observable<Task[]> {
    return this.http.get<Task[]>(this.apiUrl);
  }

  addTask(task: Task): Observable<Task> {
    return this.http.post<Task>(this.apiUrl, task);
  }

  removeTask(id: number): Observable<any> {
    return this.http.delete(`${this.apiUrl}/${id}`);
  }
  getCompletedTasks(): Observable<Task[]> {
    return this.http.get<Task[]>(this.completedTasksUrl);
  }

  clearCompletedTasks(): Observable<any> {
    return this.http.delete(this.completedTasksUrl);
  }
}
