// task.component.ts
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Task } from '../task.model';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css'],
})
export class TaskComponent {
  @Input() task: Task | undefined;
  @Output() removeTaskEvent = new EventEmitter<Task>();

  toggleCompleted(): void {
    if (this.task) {
      this.task.completed = !this.task.completed;
    }
  }

  removeTask(): void {
    if (this.task) {
      this.removeTaskEvent.emit(this.task);
    }
  }
}
